
variable "region" {
  default = "us-west-1"
}

variable "block" {
  default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  type    = list(any)
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "private_subnet_cidr" {
  type    = list(any)
  default = ["10.0.3.0/24", "10.0.4.0/24"]
}

variable "cluster_no" {

  type    = list(any)
  default = ["kafka-1", "kafka-2", "kafka-3"]
}

variable "ami" {

  type    = string
  default = "ami-01f87c43e618bf8f0"
}

variable "bastion_instance_type" {
  default = "t2.medium"
}

variable "instance_type" {

  default = "t2.2xlarge"
}

variable "instance_key" {

  type    = string
  default = "n.california"

}

variable "azs" {

  type    = list(any)
  default = ["us-west-1a", "us-west-1c"]
}

variable "ports" {
  type    = list(any)
  default = ["22", "9091", "2888", "3888", "2181", "9092"]
}



