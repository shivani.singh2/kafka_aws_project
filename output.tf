output "vpc" {

  value = aws_vpc.vpc.id

}

output "public_subnet" {

  value = aws_subnet.public_subnet.*.id
}

output "private_subnet" {

  value = aws_subnet.private_subnet.*.id
}

output "InternetGateway" {
  value = aws_internet_gateway.ig.id
}

output "nat" {
  value = aws_nat_gateway.nat.id
}

output "public-rt" {

  value = aws_route_table.public-rt.id
}

output "private-rt" {

  value = aws_route_table.private-rt.id
}

output "print_subnet" {

  value = [for name in var.public_subnet_cidr : name]
}

output "bastion_instance_id" {
  value = aws_instance.bastion_host.id
}

output "cluster_instance_id" {
  value = aws_instance.cluster.*.id
}
